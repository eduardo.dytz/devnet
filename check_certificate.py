from features.connection import setup_connection
from features.log import init_log, save_to_csv, save_to_json
from features.apic  import execute
from features.utils.model.cisco.renew_certificate import process_to_renew
from features.connection import open_connection
from features.environment import load_env
from tqdm import tqdm
import logging, time, sys


logging.getLogger("nornir").addHandler(logging.NullHandler())
logging.getLogger("nornir").propagate = False


def get_report_certificate(task,logger,path,progress_bar):

    task.host.defaults.connection_options['netmiko'].extras['session_log'] = f'/var/log/logicalis/certificates/session/{task.host}_check.log'

    task.run(
        task=open_connection,
        logger=logger,
        progress_bar=progress_bar
    )

    task.run(task=execute.check_certificate)

    task.host.close_connections()



if __name__ == "__main__":

    env_var = load_env()

    env_var['log_name'] = 'check_certificate'
    path = env_var['path']

    logger = init_log(**env_var)

    devices = setup_connection(**env_var)

    total_devices = len(devices.inventory.hosts)

    tqdm.write(f"Starting the connection with {total_devices} routers")
    time.sleep(1)

    with tqdm(total=total_devices, desc="Connecting to router",) as progress_bar:
        output = devices.run(task=get_report_certificate, logger=logger,path=path, progress_bar=progress_bar)

    data = []

    for device, results in output.items():

        if not results[0].failed:

            certificate_expire_in = results[0].host.data['certificate_expire']['certificate_expire_in']
            renew = results[0].host.data['certificate_expire']['renew']
            serial = results[0].host.data['version']['version']['chassis_sn']

            device_dict = {
                'device': device,
                'certificate_expire_in':certificate_expire_in,
                'renew':renew,
                'serial': serial
            }

            data.append(device_dict)

    save_to_json(data,**env_var)

    env_var['report_header'] = list(data[0].keys())
    save_to_csv(data,**env_var)

    to_renew = True

    if to_renew:
        process_to_renew(data,**env_var)
    else:
        sys.exit()
