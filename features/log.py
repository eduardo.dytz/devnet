import logging, csv, json
from logging.handlers import RotatingFileHandler


def level_int(level):

    if 'notset' in level:
        return 0
    if 'debug' in level:
        return 10
    if 'info' in level:
        return 20
    if 'warm' in level:
        return 30
    if 'error' in level:
        return 40
    if 'critical' in level:
        return 50


def setup_logger(name, log_file, level):

    formatter = logging.Formatter('%(levelname)s,%(asctime)s,%(message)s')

    handler = RotatingFileHandler(filename=log_file, maxBytes=10485760, backupCount=5)
    handler.setFormatter(formatter)

    log_level = level_int(level)

    logger = logging.getLogger(name)
    logger.setLevel(log_level)
    logger.addHandler(handler)

    return logger


def init_log(log_type='connection',**kwargs):

    path = kwargs['path']
    level = kwargs['log']['level']
    customer = kwargs['customer']
    log_name = kwargs['log_name']
    timestamp = kwargs['timestamp']

    output_file_name = f'/var/log/logicalis/certificates/{log_type}/{log_name}_{customer}.log'
    
    logger_name = setup_logger(name=log_name, log_file=output_file_name,level=level)

    return logger_name


def save_to_csv(dict_data,**kwargs):

    path = kwargs['path']
    customer = kwargs['customer']
    log_name = kwargs['log_name']
    timestamp = kwargs['timestamp']

    output_file_name = f"/var/log/logicalis/certificates/reports/{log_name}_{customer}_{timestamp}.csv"

    csv_columns = kwargs['report_header']
    
    try:
        with open(output_file_name, 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            writer.writeheader()
            for data in dict_data:
                writer.writerow(data)
    except IOError:
        print("I/O error")


def save_to_json(dict_data,**kwargs):

    path = kwargs['path']
    customer = kwargs['customer']
    log_name = kwargs['log_name']
    timestamp = kwargs['timestamp']

    if kwargs['log']['timestamp']:
        output_file_name = f"{path}/outputs/json/{log_name}_{customer}_{timestamp}.json"
        file_name = f'{log_name}_{customer}_{timestamp}.json'
    else:
        output_file_name = f"{path}/outputs/json/{log_name}_{customer}.json"
        file_name = f'{log_name}_{customer}.json'

    with open(output_file_name, 'w') as outfile:
        json.dump(dict_data, outfile, indent=2)

    return file_name