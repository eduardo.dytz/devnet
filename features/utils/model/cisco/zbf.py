from nornir_netmiko import netmiko_send_command

def zone_security(task):

        output = task.run(task=netmiko_send_command,
        command_string="show zone security | s INET|INET2|USERS|ATMS|ASSOC|TUNEL[1-4]0|OIMPLS", 
        use_textfsm=True
        )

        task.host['zone_security'] = output.result
