from nornir_netmiko import netmiko_send_command

def ping(task):

        dc1 = task.run(task=netmiko_send_command,
        command_string="ping vrf INET 200.186.169.73 size 1500 df",
        ).result

        dc2 = task.run(task=netmiko_send_command,
        command_string="ping vrf INET 200.186.169.73 size 1500 df"
        ).result

        task.host['ping'] = {'inet':{'dc1':{},'dc2':{}}}
        task.host['ping']['inet']['dc1'] = dc1
        task.host['ping']['inet']['dc2'] = dc2