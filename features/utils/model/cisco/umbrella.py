from nornir_netmiko import netmiko_send_command

"""
Device registration details
Interface Name          Tag              Status          Device-id 
Gi0/0/0.35              lan010718        200 SUCCESS     010a62253110aa6f
Gi0/0/0.39              SICREDI-ASSOCIAD 200 SUCCESS     010a6ec79cf770d3
Gi0/0/0.45              lan010718        200 SUCCESS     010a62253110aa6f
"""

def show_umbrella_deviceid(task):

        output = task.run(task=netmiko_send_command,
        command_string="show umbrella deviceid", 
        use_textfsm=True
        )

        task.host['umbrella_deviceid'] = output.result