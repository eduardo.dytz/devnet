from features.connection import setup_connection
from features.log import init_log, save_to_csv
from features.apic  import execute
from features.connection import open_connection
from features.apic import api_functions, functions
from tqdm import tqdm
import logging, time, sys


def get_renew_certificate(task,logger,data,progress_bar,**kwargs):

    path = kwargs['path']

    task.host.defaults.connection_options['netmiko'].extras['session_log'] = f'/var/log/logicalis/certificates/session/{task.host}_renew.log'

    task.run(task=open_connection,
        logger=logger,
        progress_bar=progress_bar
        )

    task.host['apic'] = {}
    task.host.data['apic']['platformId'] = data[task.host.name]['platformId']
    task.host.data['apic']['password'] = data[task.host.name]['password']

    task.run(task=execute.renew_certificate,**kwargs)

    task.run(task=execute.check_certificate)

    task.host.close_connections()


def process_to_renew(data,**env_var):

    env_var['log_name'] = 'process_to_renew'
    path = env_var['path']

    logger = init_log(**env_var)

    device_list = []

    other_info = {}

    for device in data:
        if device['renew']:
            device_list.append(device['device'])
            serial = device['serial']

            # Check serial
            apic = api_functions.check_certificate_by_serial(serial=serial,**env_var)

            platformId = apic['platformId']
            entityName = apic['entityName']

            # Delete certificate
            api_functions.delete_certificate_by_serial(serial,**env_var)

            # Create new certificate
            api_functions.create_new_certifcate(serial,entityName,platformId,**env_var)

            # Check serial
            apic = api_functions.check_certificate_by_serial(serial=serial,**env_var)
            trustPointId = apic['id']

            # Get new certificate
            output_dict = api_functions.get_new_certifcate(trustPointId,**env_var)

            password = output_dict['password']
            functions.download_certificate(output_dict,path,serial)

            dict_data = {
                device['device']: {
                    'platformId': platformId,
                    'password': password
                }
            }

            other_info.update(dict_data)

    if len(device_list) >= 1:
        delay_time = tqdm(total=30, desc="Delay due APIC access to router")

        for sec in range(30):
            time.sleep(1)
            delay_time.update()
    else:
        sys.exit()

    env_var['connection']['filter'] = 'host'
    env_var['connection']['host'] = device_list

    devices = setup_connection(inventory=True,threaded=True,**env_var)

    total_devices = len(devices.inventory.hosts)

    tqdm.write(f"Starting the connection with {total_devices} routers")
    time.sleep(1)

    with tqdm(total=total_devices, desc="Connecting to router",) as progress_bar:
        output = devices.run(task=get_renew_certificate, data=other_info,logger=logger,progress_bar=progress_bar,**env_var)

    data = []

    for device, results in output.items():

        if not results[0].failed:

            certificate_expire_in = results[0].host.data['certificate_expire']['certificate_expire_in']
            renew = results[0].host.data['certificate_expire']['renew']
            serial = results[0].host.data['version']['version']['chassis_sn']

            device_dict = {
                'device': device,
                'certificate_expire_in':certificate_expire_in,
                'renew':renew,
                'serial': serial
            }

            data.append(device_dict)

    env_var['report_header'] = list(data[0].keys())
    save_to_csv(data,**env_var)
