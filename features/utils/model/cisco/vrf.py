from nornir_netmiko import netmiko_send_command

def show_vrf(task):

        output = task.run(task=netmiko_send_command,
        command_string="show vrf", 
        use_genie=True
        )

        task.host['vrf'] = output.result
