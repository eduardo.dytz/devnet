from nornir_netmiko import netmiko_send_command

def show_bgp_all_summary(task):

        output = task.run(task=netmiko_send_command,
        command_string="show bgp all summary", 
        use_genie=True
        )

        task.host['bgp_summary'] = output.result
