from nornir_netmiko import netmiko_save_config, netmiko_send_config, netmiko_send_command

def save(task):

    task.run(
        task=netmiko_save_config,
        cmd='wr',
        confirm=True
    )

def send_config(task,config_file):

    if isinstance(config_file,str):

        task.run(
            task=netmiko_send_config,
            config_file=config_file
        )

    else:
        task.run(
            task=netmiko_send_config,
            config_commands=config_file
        )