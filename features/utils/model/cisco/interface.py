from nornir_netmiko import netmiko_send_command

def show_interface(task):

        output = task.run(task=netmiko_send_command,
        command_string="show interfaces", 
        use_genie=True
        )

        task.host['interface'] = output.result


def show_ip_interface_brief(task):

        output = task.run(task=netmiko_send_command,
        command_string="show ip interface brief", 
        use_genie=True
        )

        task.host['interface_brief'] = output.result