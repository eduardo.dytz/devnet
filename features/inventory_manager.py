from features.log import init_log
from jinja2 import Environment, FileSystemLoader
from requests.auth import HTTPBasicAuth
from tqdm import tqdm
import requests, time, json, urllib3, sys, csv


urllib3.disable_warnings()


def init_jinja(file):

    file_loader = FileSystemLoader(f'{path}/features/templates')
    env = Environment(loader=file_loader)
    template = env.get_template(file)

    return template


def get_device_pages(**kwargs):

    ip = kwargs['prime']['ip']
    username = kwargs['prime']['username']
    password = kwargs['prime']['password']
    headers = kwargs['headers']
    params = kwargs['params']

    url = "https://{}/webacs/api/v4/data/Devices.json".format(ip)

    response = requests.request("GET", url, headers=headers,params=params, auth=HTTPBasicAuth(username, password), verify=False)
    if response.status_code != 200:
        print(f"Error to connect to Cisco Prime with status code {response.status_code}")
        sys.exit()
    else:
        pass

    response_json = response.json()
    total_devices = response_json['queryResponse']['@count']

    total_pages = int(total_devices) // 1000
    total_pages_rest = int(total_devices) % 1000

    if total_pages == 0:
        pages = 1
    elif total_pages_rest > 0:
        pages = total_pages + 1
    else:
        pages = total_pages

    tqdm.write(f"Connected to Cisco Prime and identified {total_devices} routers.")
    time.sleep(1)

    return pages

def get_prime_device_list(**kwargs):

    data = []

    headers = {
    'Content-Type': 'application/json'
    }

    params = {
    "productFamily": "Routers",
    "deviceType": 'contains("Integrated")',
    "ipAddress": 'startsWith("10.248")',
    ".full": "true",
    ".maxResults": 1000
    }

    ip_prime = kwargs['prime']['ip']
    username = kwargs['prime']['username']
    password = kwargs['prime']['password']
    kwargs['headers'] = headers
    kwargs['params'] = params

    pages = get_device_pages(**kwargs)

    firstResult = 0

    for page in range(pages):

        params['.firstResult'] = firstResult
        url = "https://{}/webacs/api/v4/data/Devices.json".format(ip_prime)
        response = requests.request("GET", url, headers=headers,params=params, auth=HTTPBasicAuth(username, password), verify=False)
        response_json = response.json()
        response_output = response_json['queryResponse']['entity']
        for each_response in response_output:
            data.append(each_response['devicesDTO'])
        firstResult+=1000

    return data


def get_csv_device_list(**kwargs):

    path = kwargs['path']
    customer = kwargs['customer'] 

    reader = csv.DictReader(open(f'{path}/files/device_list/{customer}.csv', 'r'))
    data = []
    for line in reader:
        data.append(line)

    return data


def get_inventory(threaded,**kwargs):

    global path

    path = kwargs['path']

    update_inventory = kwargs['connection']['update_inventory']

    if update_inventory == 'prime':
        data = get_prime_device_list(**kwargs)
    else:
        data = get_csv_device_list(**kwargs)

    create_hosts(data,**kwargs)
    create_groups(**kwargs)
    create_defaults(**kwargs)
    create_confg(threaded,**kwargs)


def prime_parser(data,logger,**kwargs):

    customer = kwargs['customer']

    output = []

    with tqdm(total=len(data), desc="Creating hosts inventory",) as host_progress_bar:

        for device in data:

            hostname = device['deviceName'].split('.')[0]
            ip = device['ipAddress']
            platform = 'cisco_xe'
            coop = hostname.split('-')[2]
            tag = f'C{coop}'

            device_dict = {
                'hostname': hostname,
                'ip': ip,
                'platform': platform,
                'tag': tag,
                'customer': customer
            }

            host_progress_bar.update()
            logger.info(f"{hostname},{ip},{tag}")

            output.append(device_dict)

    return output


def csv_parser(data,logger,**kwargs):

    customer = kwargs['customer']

    output = []

    with tqdm(total=len(data), desc="Creating hosts inventory",) as host_progress_bar:

        for device in data:

            hostname = device['hostname']
            ip = device['ip']
            platform = device['platform']
            tag = device['tag']

            device_dict = {
                'hostname': hostname,
                'ip': ip,
                'platform': platform,
                'tag': tag,
                'customer': customer
            }

            host_progress_bar.update()
            logger.info(f"{hostname},{ip},{tag}")

            output.append(device_dict)

    return output


def create_hosts(data,**kwargs):

    kwargs['log_name'] = 'import_hosts'
    kwargs['log']['level'] = 'info'
    logger = init_log(log_type='import',**kwargs)

    template = init_jinja('hosts.j2')

    update_inventory = kwargs['connection']['update_inventory']

    if update_inventory == 'prime':
        output = prime_parser(data,logger,**kwargs)
    else:
        output = csv_parser(data,logger,**kwargs)

    hosts_file = template.render(data=output)
    file = open(f"{path}/features/inventory/hosts.yaml","w")
    file.write(str(hosts_file))
    file.close()

    time.sleep(1)


def create_groups(**kwargs):

    data = {}
    data['tacacs_username'] = kwargs['tacacs']['username']
    data['tacacs_password'] = kwargs['tacacs']['password']
    data['local_username'] = kwargs['local']['username']
    data['local_password'] = kwargs['local']['password']
    data['secret'] = kwargs['local']['secret']
    data['customer'] = kwargs['customer']

    template = init_jinja('groups.j2')
    groups_file = template.render(data=data)
    file = open(f"{path}/features/inventory/groups.yaml","w")
    file.write(str(groups_file))
    file.close()

def create_defaults(**kwargs):

    data = {}
    data['local_username'] = kwargs['local']['username']
    data['local_password'] = kwargs['local']['password']
    data['secret'] = kwargs['local']['secret']
    data['customer'] = kwargs['customer']

    template = init_jinja('defaults.j2')
    defaults_file = template.render(data=data)
    file = open(f"{path}/features/inventory/defaults.yaml","w")
    file.write(str(defaults_file))
    file.close()


def create_confg(threaded,**kwargs):

    data = {}

    if threaded:
        data['runner'] = 'threaded'
    else:
        data['runner'] = 'serial'

    data['num_workers'] = kwargs['connection']['num_workers']
    data['path'] = path

    template = init_jinja('config.j2')
    config_file = template.render(data=data)
    file = open(f"{path}/features/config.yaml","w")
    file.write(str(config_file))
    file.close()