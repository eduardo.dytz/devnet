from jinja2 import Environment, FileSystemLoader
import json, glob, os

def config_maker(data,**kwargs):

    path = kwargs['path']
    template = kwargs['template']

    files = glob.glob(f'{path}/outputs/configs/rt1-*')
    
    for f in files:
        os.remove(f)

    file_loader = FileSystemLoader(f'{path}/templates')
    env = Environment(loader=file_loader)
    template = env.get_template(template)

    if isinstance(data,str):
        with open(f'{path}/outputs/json/{data}','r') as json_file:
            data = json.load(json_file)

    for device in data:

        device_name = device['device']
        output = template.render(data=device)

        file = open(f"{path}/outputs/configs/{device_name}.ios","w")
        file.write(str(output))
        file.close()