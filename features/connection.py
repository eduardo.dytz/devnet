from nornir import InitNornir
from nornir.core.filter import F
from nornir.core.exceptions import NornirSubTaskError
from netmiko.ssh_exception import NetMikoAuthenticationException, NetMikoTimeoutException
from paramiko.ssh_exception import AuthenticationException
from nornir_netmiko import netmiko_send_command
from tqdm import tqdm
from features.inventory_manager import get_inventory
import re


def reset_connection(host):

    try:
        host.close_connections()
    except ValueError:
        pass


def setup_connection(inventory=True,threaded=True,**kwargs):

    if inventory:
        get_inventory(threaded,**kwargs)

    path = kwargs['path']
    filter_type = kwargs['connection']['filter']

    nr = InitNornir(config_file=f"{path}/features/config.yaml")

    if 'tag' == filter_type:
        tag_list = kwargs['connection']['tag']
        return nr.filter(filter_func=lambda h: h.data['tag'] in tag_list)

    elif 'host' == filter_type:
        host_list = kwargs['connection']['host']
        return nr.filter(filter_func=lambda h: h.name in host_list)

    else:
        customer = kwargs['customer']
        return nr.filter(F(groups__contains=customer))


def pattern_nomalization(text=False,**kwargs):

    re_specials = re.compile(r'[^A-Za-z0-9 ]+')
    re_space = re.compile(r'\s+')

    if text:
        text = re.sub(re_specials, '', text)
        text = re.sub(re_space, '', text)

        return text

    else:

        patterns = []
        patterns_raw = kwargs['interface_filter']['patterns']

        for pattern_raw in patterns_raw:
            pattern_raw = re.sub(re_specials, '', pattern_raw)
            pattern_raw = re.sub(re_space, '', pattern_raw)
            patterns.append(pattern_raw)

        return patterns

def open_connection(task,logger,progress_bar):

    try:
        r = task.run(task=netmiko_send_command,
        command_string="show version", 
        use_genie=True
        )

        task.host['version'] = r.result

        tqdm.write(f"{task.host}: connect to router")
        logger.info(f"{task.host},{task.host.hostname},login with tacacs")

    except NornirSubTaskError as e:

        if isinstance(e.result.exception, AuthenticationException):
            # Remove the failed result
            task.results.pop()
            reset_connection(task.host)
            # Try again
            task.host.username = task.host.groups[0].defaults.username
            task.host.password = task.host.groups[0].defaults.password

            try:
                r = task.run(task=netmiko_send_command,
                command_string="show version", 
                use_genie=True
                )

                task.host['version'] = r.result

                tqdm.write(f"{task.host}: connect to router with local user")
                logger.info(f"{task.host},{task.host.hostname},login with local user")

            except Exception:
                tqdm.write(f"{task.host}: authentication error")
                logger.error(f"{task.host},{task.host.hostname},authentication error")

        else:
            tqdm.write(f"{task.host}: timeout")
            logger.error(f"{task.host},{task.host.hostname},timeout")

    progress_bar.update()