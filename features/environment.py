import os, datetime, yaml, sys, inspect, glob, time


def create_folders(path):

    dir_list = [
        'features/inventory',
        'files/configs',
        'outputs/json',
        'outputs/certificates'
    ]

    log_list = [
        '/var/log/logicalis/certificates/connection',
        '/var/log/logicalis/certificates/import',
        '/var/log/logicalis/certificates/session',
        '/var/log/logicalis/certificates/reports'
    ]

    for file_dir in dir_list:
        try:
            os.makedirs(f'{path}/{file_dir}')
        except FileExistsError:
            pass

    for log_dir in log_list:
        try:
            os.makedirs(log_dir)
        except FileExistsError:
            pass

    dir_report = '/var/log/logicalis/certificates/reports'

    now = time.time()
    list_of_files = filter(os.path.isfile, glob.glob(f'{dir_report}/*'))

    for file_path in list_of_files:
        if os.stat(file_path).st_mtime < now - 90 * 86400:
            os.remove(file_path)


def set_ntc_environment(path):

    os.environ["NET_TEXTFSM"] = f"{path}/features/templates/ntc_templates/templates"


def load_env():

    currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    parentdir = os.path.dirname(currentdir)

    with open(f'{parentdir}/environment.yaml') as file_config:
        env_var = yaml.load(file_config, Loader=yaml.FullLoader)

    env_var = validate_env_var(**env_var)

    create_folders(parentdir)

    env_var['path'] = parentdir
    env_var['timestamp'] = timestamp()

    set_ntc_environment(parentdir)

    return env_var


def check_key(data,key):

    if key in data:
        if data[key] is not None:
            pass
        else:
            print(f"No value to parameter {key} on envionment.yaml")
            sys.exit()
    else:
        print(f"No parameter {key} on envionment.yaml")
        sys.exit()


def validate_env_var(**kwargs):

    check_key(kwargs,'customer')

    check_key(kwargs,'tacacs')
    check_key(kwargs['tacacs'],'username')
    check_key(kwargs['tacacs'],'password')

    check_key(kwargs,'connection')
    check_key(kwargs['connection'],'num_workers')
    check_key(kwargs['connection'],'update_inventory')

    if kwargs['connection']['update_inventory'] == 'prime':
        check_key(kwargs,'prime')
        check_key(kwargs['prime'],'ip')
        check_key(kwargs['prime'],'username')
        check_key(kwargs['prime'],'password')

    check_key(kwargs['connection'],'filter')
    if 'host' == kwargs['connection']['filter']:
         check_key(kwargs['connection'],'host')

    elif 'tag' == kwargs['connection']['filter']:
        check_key(kwargs['connection'],'tag')

    else:
        kwargs['connection']['filter'] = 'customer'

    if 'interface_filter' in kwargs:
        check_key(kwargs,'interface_filter')
        check_key(kwargs['interface_filter'],'patterns')

    check_key(kwargs,'log')
    check_key(kwargs['log'],'level')
    check_key(kwargs['log'],'timestamp')

    return kwargs


def timestamp():

    dateTimeObj = datetime.datetime.now()
    timestamp = dateTimeObj.strftime("%d%b%Y_%H%M%S")

    return timestamp