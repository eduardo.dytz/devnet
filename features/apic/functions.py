from nornir_netmiko import netmiko_file_transfer, netmiko_send_config, netmiko_send_command
import wget
import datetime
import requests
import os
import glob



def download_certificate(output_dict,path,serial):

    url = output_dict['url']
    url_full = f'http://172.19.175.17{url}'

    if os.path.exists(f'{path}/outputs/certificates/{serial}.pfx'):
        os.remove(f'{path}/outputs/certificates/{serial}.pfx')
    wget.download(url_full,out=f'{path}/outputs/certificates/{serial}.pfx')


def get_certificate_info(task):

    task.host["certificate"] = task.run(task=netmiko_send_command,
        command_string="show crypto pki certificates sdn-network-infra-iwan", 
        use_genie=True
        ).result


def copy_ftp_to_device(task,path,serial):

    task.run(task=netmiko_file_transfer,
        name='copy_ftp_to_device',
        source_file=f'{path}/outputs/certificates/{serial}.pfx', 
        dest_file=f'{serial}.pfx',
        overwrite_file=True
    )


def unconfig_certificate(task):

    task.run(task=netmiko_send_command,
        command_string='config t\nno crypto pki trustpoint sdn-network-infra-iwan\nyes',
        use_timing=True
        )

    task.run(task=netmiko_send_command,
        command_string='crypto key zeroize rsa sdn-network-infra-iwan\nyes',
        use_timing=True
        )


def install_certificate(task,serial,password):

    commands = f'crypto pki import sdn-network-infra-iwan pkcs12 bootflash:{serial}.pfx password {password}\n\n'

    task.run(task=netmiko_send_command,
        command_string=commands,
        use_timing=True
        )


def configure_certificate(task,platformId,serial):

    commands = [
        'crypto pki trustpoint sdn-network-infra-iwan',
        'enrollment url http://172.19.175.17:80/ejbca/publicweb/apply/scep/sdnscep',
        f'fqdn {task.host}',
        f'subject-name CN={platformId}_{serial}_sdn-network-infra-iwan',
        'revocation-check none',
        'source interface Loopback0',
        'rsakeypair sdn-network-infra-iwan',
        'auto-enroll 80 regenerate'
        ]

    task.run(task=netmiko_send_config,
        config_commands=commands
        )


def renew(dt):

    dt_split = dt.split()

    ano = dt_split[-1]
    dia = dt_split[-2]
    mes_name = dt_split[-3]
    datetime_object = datetime.datetime.strptime(mes_name, "%b")
    mes_number = datetime_object.month

    expire_date_format = datetime.datetime(int(ano), int(mes_number), int(dia))

    today = datetime.date.today()

    differ_days = expire_date_format.date() - today

    if differ_days.days < 60:
        return True
    else:
        return False
