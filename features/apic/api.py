import requests
import json
import sys


def get_X_auth_token(ip,api_version,username,password):
    r_json = {
    "username": username,
    "password": password
    }
    post_url = "https://"+ip+"/api/"+api_version+"/ticket"
    headers = {'content-type': 'application/json'}
    try:
        r = requests.post(post_url, data = json.dumps(r_json), headers=headers,verify=False)
        # print (r.text)
        return r.json()["response"]["serviceTicket"]
    except:
        print ("Status: %s"%r.status_code)
        print ("Response: %s"%r.text)
        sys.exit ()


def get(ip,api_version,username,password,api='',params=''):
    ticket = get_X_auth_token(ip,api_version,username,password)
    headers = {"X-Auth-Token": ticket}
    url = "https://"+ip+"/api/"+api_version+"/"+api
    print ("Executing GET '%s'"%url)

    try:
        resp= requests.get(url,headers=headers,params=params,verify=False)
        print ("GET {} Status: {}".format(api,resp.status_code))
        return(resp)
    except:
       print ("Something wrong with GET /",api)
       sys.exit()


def post(ip,api_version,username,password,api='',data=''):
    ticket = get_X_auth_token(ip,api_version,username,password)
    headers = {"content-type" : "application/json","X-Auth-Token": ticket}
    url = "https://"+ip+"/api/"+api_version+"/"+api
    print ("Executing POST '%s'"%url)

    try:
        resp= requests.post(url,json.dumps(data),headers=headers,verify = False)
        print ("POST {} Status: {}".format(api,resp.status_code))
        return(resp)
    except:
       print ("Something wrong with POST /",api)
       sys.exit()


def put(ip,api_version,username,password,api='',data=''):
    ticket = get_X_auth_token(ip,api_version,username,password)
    headers = {"content-type" : "application/json","X-Auth-Token": ticket}
    url = "https://"+ip+"/api/"+api_version+"/"+api
    print ("Executing PUT '%s'"%url)
    try:
        resp= requests.put(url,json.dumps(data),headers=headers,verify = False)
        print ("PUT {} Status: {}".format(api,resp.status_code))
        return(resp)
    except:
       print ("Something wrong with PUT /",api)
       sys.exit()


def delete(ip,api_version,username,password,api='',params=''):
    ticket = get_X_auth_token(ip,api_version,username,password)
    headers = {"content-type" : "application/json","X-Auth-Token": ticket}
    url = "https://"+ip+"/api/"+api_version+"/"+api
    print ("Executing DELETE '%s'"%url)
    try:
        resp= requests.delete(url,headers=headers,params=params,verify = False)
        print ("DELETE {} Status: {}".format(api,resp.status_code))
        return(resp)
    except:
       print ("Something wrong with DELETE /",api)
       sys.exit()
