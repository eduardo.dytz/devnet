from features.apic import functions
from features.apic import api_functions
from features.utils.model.cisco.functions import save


def check_certificate(task):

    task.run(task=functions.get_certificate_info)

    if len(task.host.data['certificate']) >= 1:
        end_date = task.host.data['certificate']['trustpoints']['sdn-network-infra-iwan']['associated_trustpoints']['certificate']['validity_date']['end_date']
        renew_in = functions.renew(end_date)
    else:
        end_date = None
        renew_in = True

    device_dict = {
        'certificate_expire_in':end_date,
        'renew': renew_in
        }

    task.host['certificate_expire'] = device_dict


def renew_certificate(task,**kwargs):

    path = kwargs['path']

    serial = task.host.data['version']['version']['chassis_sn']

    platformId = task.host.data['apic']['platformId']


    functions.copy_ftp_to_device(task,path,serial)

    task.run(task=functions.unconfig_certificate)

    password = task.host["apic"]['password']
    task.run(task=functions.install_certificate,serial=serial,password=password)

    task.run(task=functions.configure_certificate,platformId=platformId,serial=serial)

    task.run(task=save)
