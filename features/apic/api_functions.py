from features.apic.api import get, post, delete


def check_certificate_by_serial(serial,**kwargs):

    ip = kwargs['apic']['ip']
    username = kwargs['apic']['username']
    password = kwargs['apic']['password']

    api_version = 'v1'
    api = f'trust-point/serial-number/{serial}'

    response = get(ip,api_version,username,password,api)

    response_dict = response.json()["response"]

    return response_dict


def delete_certificate_by_serial(serial,**kwargs):

    ip = kwargs['apic']['ip']
    username = kwargs['apic']['username']
    password = kwargs['apic']['password']

    api_version = 'v1'
    api = f'trust-point/serial-number/{serial}'

    response = delete(ip,api_version,username,password,api)

    response_dict = response.json()["response"]

    return response_dict


def create_new_certifcate(serial,entityName,platformId,**kwargs):

    ip = kwargs['apic']['ip']
    username = kwargs['apic']['username']
    password = kwargs['apic']['password']

    data = {
        'entityName': entityName,
        'serialNumber': serial,
        'platformId': platformId,
        'trustProfileName': 'sdn-network-infra-iwan'
    }

    api_version = 'v1'
    api = f'trust-point'

    response = post(ip,api_version,username,password,api,data)

    response_dict = response.json()["response"]

    return response_dict


def get_new_certifcate(trustPointId,**kwargs):

    ip = kwargs['apic']['ip']
    username = kwargs['apic']['username']
    password = kwargs['apic']['password']

    api_version = 'v1'
    api = f'trust-point/{trustPointId}/config'

    response = get(ip,api_version,username,password,api)

    certificate_url = response.json()["response"]['pkcs12Url']
    certificate_password = response.json()["response"]['pkcs12Password']

    output_dict = {
        'url': certificate_url,
        'password': certificate_password
    }

    return output_dict