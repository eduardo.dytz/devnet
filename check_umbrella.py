from features.connection import setup_connection
from features.utils.model.cisco import umbrella
from features.log import init_log, save_to_csv, save_to_json
from features.connection import open_connection
from features.environment import load_env
from tqdm import tqdm
import logging, time, sys


logging.getLogger("nornir").addHandler(logging.NullHandler())
logging.getLogger("nornir").propagate = False


def exec_ping(task,logger,progress_bar):

    task.host.defaults.connection_options['netmiko'].extras['session_log'] = f'/var/log/logicalis/certificates/session/{task.host}_check.log'

    task.run(
        task=open_connection,
        logger=logger,
        progress_bar=progress_bar
    )

    task.run(task=umbrella.show_umbrella_deviceid)

    task.host.close_connections()


if __name__ == "__main__":

    env_var = load_env()

    env_var['log_name'] = 'check_umbrella'
    path = env_var['path']

    logger = init_log(**env_var)

    devices = setup_connection(**env_var)

    total_devices = len(devices.inventory.hosts)

    tqdm.write(f"Starting the connection with {total_devices} routers")
    time.sleep(1)

    with tqdm(total=total_devices, desc="Connecting to router",) as progress_bar:
        output = devices.run(task=exec_ping, logger=logger, progress_bar=progress_bar)

    data = []

    for device, results in output.items():

        if not results[0].failed:

            umbrella = results[0].host.data['umbrella_deviceid']

            if isinstance(umbrella,list):
                for intf in umbrella:
                    if intf['umbrella_status'] != '200':
                        intf['status'] = 'NOK'
                    else:
                        intf['status'] = 'OK'

                    device_dict = {'device':device}
                    device_dict.update(intf)
                    data.append(device_dict)

            else:
                device_dict = {
                    "device":device,
                    "intf": None,
                    "tag": None,
                    "umbrella_status": None,
                    "status": "NOK"
                }

                data.append(device_dict)

    save_to_json(data,**env_var)

    env_var['report_header'] = list(data[0].keys())
    save_to_csv(data,**env_var)
