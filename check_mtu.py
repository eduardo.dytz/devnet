from features.connection import setup_connection
from features.utils.model.cisco import ping
from features.log import init_log, save_to_csv, save_to_json
from features.connection import open_connection
from features.environment import load_env
from tqdm import tqdm
import logging, time, sys


logging.getLogger("nornir").addHandler(logging.NullHandler())
logging.getLogger("nornir").propagate = False


def exec_ping(task,logger,progress_bar):

    task.host.defaults.connection_options['netmiko'].extras['session_log'] = f'/var/log/logicalis/certificates/session/{task.host}_check.log'

    task.run(
        task=open_connection,
        logger=logger,
        progress_bar=progress_bar
    )

    task.run(task=ping.ping)

    task.host.close_connections()



if __name__ == "__main__":

    env_var = load_env()

    env_var['log_name'] = 'check_mtu'
    path = env_var['path']

    logger = init_log(**env_var)

    devices = setup_connection(**env_var)

    total_devices = len(devices.inventory.hosts)

    tqdm.write(f"Starting the connection with {total_devices} routers")
    time.sleep(1)

    with tqdm(total=total_devices, desc="Connecting to router",) as progress_bar:
        output = devices.run(task=exec_ping, logger=logger, progress_bar=progress_bar)

    data = []

    for device, results in output.items():

        device_dict = {'device':device}

        if not results[0].failed:

            ping = results[0].host.data['ping']['inet']

            try:
                ping_inet_dc1 = ping['dc1'].splitlines()
            except Exception:
                ping_inet_dc1 = ['U','']

            try: 
                ping_inet_dc2 = ping['dc2'].splitlines()
            except Exception:
                ping_inet_dc2 = ['U','']

            if len(ping_inet_dc1) > 1:
                if 'M' in ping_inet_dc1[-2]:
                    result_dc1 = 'NOK'
                elif 'U' in ping_inet_dc1[-2]:
                    result_dc1 = 'Unreacheble'
                else:
                    result_dc1 = 'Ok'
            else:
                result_dc1 = 'down'

            if len(ping_inet_dc2) > 1:
                if 'M' in ping_inet_dc2[-2]:
                    result_dc2 = 'NOK'
                elif 'U' in ping_inet_dc2[-2]:
                    result_dc2 = 'Unreacheble'
                else:
                    result_dc2 = 'Ok'
            else:
                result_dc2 = 'down'

            device_dict = {
                'device': device,
                'vrf':'inet',
                'dc1':result_dc1,
                'dc2': result_dc2
            }

            data.append(device_dict)


    save_to_json(data,**env_var)

    env_var['report_header'] = list(data[0].keys())
    save_to_csv(data,**env_var)
